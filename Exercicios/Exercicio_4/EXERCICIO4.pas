unit EXERCICIO4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    edit: TButton;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure editClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);

  private
    { Private declarations  }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
if Edit1.Text='' then begin
Application.MessageBox('Por Favor insira um texto','Aviso',mb_Ok+mb_IconExclamation);
end else
ListBox1.Items.Add(Edit1.Text);
Edit1.Text:='';
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
ListBox1.Items.Clear;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
listbox1.Items.SaveToFile('C:');
end;

procedure TForm1.editClick(Sender: TObject);
begin
 ListBox1.items[ListBox1.ItemIndex]:=Edit1.Text;
 Edit1.Text:='';
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
edit.Enabled:=false;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
begin
 if ListBox1.ItemIndex>-1 then begin
edit.Enabled:=True;
Edit1.Text:=ListBox1.items[ListBox1.ItemIndex];
end else begin
  edit.Enabled:=false;
  end;
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
Listbox1.DeleteSelected;
end;

end.
