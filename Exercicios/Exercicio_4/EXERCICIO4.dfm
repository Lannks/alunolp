object Form1: TForm1
  Left = 195
  Top = 110
  Caption = 'Form1'
  ClientHeight = 361
  ClientWidth = 189
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 16
    Top = 80
    Width = 161
    Height = 137
    ItemHeight = 13
    TabOrder = 0
    OnClick = ListBox1Click
  end
  object Edit1: TEdit
    Left = 16
    Top = 8
    Width = 161
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 16
    Top = 40
    Width = 161
    Height = 25
    Caption = 'INSERIR'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 16
    Top = 232
    Width = 161
    Height = 25
    Caption = 'EXCLUIR'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 16
    Top = 265
    Width = 161
    Height = 25
    Caption = 'EXCLUIR TUDO'
    TabOrder = 4
    OnClick = Button3Click
  end
  object edit: TButton
    Left = 16
    Top = 296
    Width = 161
    Height = 25
    Caption = 'EDITAR'
    TabOrder = 5
    OnClick = editClick
  end
  object Button4: TButton
    Left = 16
    Top = 328
    Width = 161
    Height = 25
    Caption = 'SALVAR'
    TabOrder = 6
    OnClick = Button4Click
  end
end
